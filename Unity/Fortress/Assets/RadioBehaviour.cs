﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MessagingLibrary;

public class RadioBehaviour : MonoBehaviour {
    private Emitter mEmitter;
    private bool startedPlayingSound = false;

    // Use this for initialization
    void Start()
    {
        mEmitter = new Emitter(FortressBroadcaster.GetInstance());
	}
	
	// Update is called once per frame
	void Update () {
		if(!startedPlayingSound)
        {
            startedPlayingSound = true;
            Message messageToEmit = FortressSoundFactory.GetSoundMessage(SOUNDTYPE.MUSIC, 2, transform.position, true, "radio");
            mEmitter.Emit(messageToEmit);
        }
	}
}
