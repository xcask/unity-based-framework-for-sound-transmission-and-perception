﻿using UnityEngine;
using MessagingLibrary;
using MessagingLibrary.Filters;
using System;

public class FocusFilterBehaviour : AbstractFilterBehaviour
{
    FocusFilter focusFilter;

    [Range(1, 5)]
    public int focusLevel;

    void Awake()
    {
        focusFilter = new FocusFilter();
        focusFilter.ChangeFocusLevel(focusLevel);
    }

    void Update()
    {
        
    }

    public void ChangeFocusLevel(int focusLevel)
    {
        this.focusLevel = focusLevel;
        focusFilter.ChangeFocusLevel(focusLevel);
    }

    public override Filter GetFilter()
    {
        return focusFilter;
    }
}