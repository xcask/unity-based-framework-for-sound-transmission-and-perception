﻿using UnityEngine;
using MessagingLibrary;
using MessagingLibrary.Filters;
using System;

public class HearingLossFilterBehaviour : AbstractFilterBehaviour
{
    AgeFilter ageFilter;

    [Range(18, 100)]
    public int age;

    void Awake()
    {
        ageFilter = new AgeFilter();
        ageFilter.SetAge(age);
    }

    void Update()
    {
        //the listener is not expected to grow old during a game session...
    }

    public override Filter GetFilter()
    {
        return ageFilter;
    }
}