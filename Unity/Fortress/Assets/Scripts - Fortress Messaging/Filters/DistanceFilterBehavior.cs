﻿using UnityEngine;
using System.Collections;
using MessagingLibrary;
using MessagingLibrary.Filters;

public class DistanceFilterBehaviour : AbstractFilterBehaviour, DistanceFalloffFilter.PositionProvider
{
    DistanceFalloffFilter filter;
    Vector3 position = new Vector3();

    // Use this for initialization
    void Awake()
    {
        if (filter == null)
        {
            filter = new DistanceFalloffFilter();
        }

        filter.SetPositionProvider(this);
    }

    // Update is called once per frame
    void Update()
    {

    }

    Vector3 GetPosition()
    {
        if (gameObject != null)
        {
            position.x = gameObject.transform.position.x;
            position.y = gameObject.transform.position.y;
            position.z = gameObject.transform.position.z;
        }

        return position;
    }

    public override Filter GetFilter()
    {
        if (filter == null)
        {
            filter = new DistanceFalloffFilter();
        }

        return filter;
    }

    Vector3 DistanceFalloffFilter.PositionProvider.GetPosition()
    {
        return transform.position;
    }
}