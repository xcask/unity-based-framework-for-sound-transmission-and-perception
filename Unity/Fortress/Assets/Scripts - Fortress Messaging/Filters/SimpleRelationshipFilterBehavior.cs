﻿using UnityEngine;
using System.Collections;
using MessagingLibrary;
using MessagingLibrary.Filters;

public class SimpleRelationshipFilterBehavior : AbstractFilterBehaviour, Filter
{

    [Range(1, 3)]
    public int importanceLevel;

    public override Filter GetFilter()
    {
        return this;
    }

    public float ComputeSalience(Message message, float currentSalience)
    {
        return currentSalience * GetImportanceLevel(importanceLevel);
    }

    public float GetImportanceLevel(int importanceLevel)
    {
        switch(importanceLevel)
        {
            case 1:
                return 0.8f;
            case 2:
                return 0.9f;
            case 3:
                return 1.0f;
            default:
                return 1.0f;
        }
    }
}
