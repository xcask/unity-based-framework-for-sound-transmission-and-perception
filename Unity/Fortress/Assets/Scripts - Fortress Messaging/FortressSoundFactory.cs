﻿using UnityEngine;
using System.Collections;
using MessagingLibrary;

public class FortressSoundFactory
{
    public static string TRANSCRIPT_FOOTSTEP = "footstep";
    public static string TRANSCRIPT_JUMP = "jump";
    public static string TRANSCRIPT_JUMP_LANDING = "jump_landing";
    public static string TRANSCRIPT_LOUD_MUSIC = "loud_music";

	public static Message GetSoundMessage(SOUNDTYPE soundType, int emitterId, Vector3 emitterPosition, bool isNoise)
	{
		return GetSoundMessage(soundType, emitterId, emitterPosition, isNoise, null);
	}

	public static Message GetSoundMessage(SOUNDTYPE soundType, int emitterId, Vector3 emitterPosition, bool isNoise, string transcript)
    {
        Message message = new Message();

        message.EmmiterId = emitterId;
		message.EmmiterPosition = emitterPosition;
        message.IsNoise = isNoise;

        switch (soundType)
        {
		case SOUNDTYPE.FOOTSTEP: {
                message.frequency = FrequencyRange.FourK;
                message.Intensity = 60f;
				if(transcript == null)
					message.Transcript = TRANSCRIPT_FOOTSTEP;
                break;
			}

            case SOUNDTYPE.JUMP:
			{
                message.frequency = FrequencyRange.ShoesOnConcrete;
                message.Intensity = 55f;
				if(transcript == null)
					message.Transcript = TRANSCRIPT_JUMP;
			}
                break;

            case SOUNDTYPE.JUMP_LANDING:
			{
                message.frequency = FrequencyRange.ShoesOnConcrete;
                message.Intensity = 55f;
				if(transcript == null)
					message.Transcript = TRANSCRIPT_JUMP_LANDING;

                break;
			}
            case SOUNDTYPE.MUSIC:
			{
                message.frequency = FrequencyRange.Music;
                message.Intensity = 60f;
				if(transcript == null)
					message.Transcript = TRANSCRIPT_LOUD_MUSIC;

                break;
			}
            case SOUNDTYPE.SPEECH:
			{
                message.frequency = FrequencyRange.MaleVoice;
                message.Intensity = 45f;

                break;
			}
            case SOUNDTYPE.SHOUT:
			{
                message.frequency = FrequencyRange.MaleVoice;
                message.Intensity = 60f;

                break;
			}
            default:

                break;
        }

		if (transcript != null)
			message.Transcript = transcript;

        return message;
    }
}

public enum SOUNDTYPE { FOOTSTEP, JUMP, JUMP_LANDING, MUSIC, SPEECH, SHOUT };
