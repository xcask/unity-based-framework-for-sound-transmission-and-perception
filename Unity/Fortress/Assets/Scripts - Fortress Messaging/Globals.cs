﻿using UnityEngine;
using System.Collections;

public class FortressGlobals
{
    public const int ENTITY_ID_PLAYER = 1;
    public const int ENTITY_ID_NPC_FRIENDLY = 2;
    public const int ENTITY_ID_NPC_ENEMY = 3;
}
