﻿using System;
using System.Collections.Generic;

using MessagingLibrary;
using UnityEngine;

public class FortressBroadcaster : Broadcaster
{
    private const float MINIMUM_PERCEPTIBLE_INTENSITY = 42;

    private static FortressBroadcaster singleton;
	public new static FortressBroadcaster GetInstance()
	{
		if (singleton == null) {
			singleton = new FortressBroadcaster();
		}

		return singleton;
	}

	Dictionary<Listener, Transform> listenersTransforms;

    private FortressBroadcaster()
	{
		listenersTransforms = new Dictionary<Listener, Transform>();
	}

	/**
     * Deliver the message to all the registered listeners.
     */
	public override void Broadcast(Message message)
	{
        if (message.Transcript == FortressSoundFactory.TRANSCRIPT_JUMP_LANDING || message.Transcript == FortressSoundFactory.TRANSCRIPT_JUMP)
            return;
        //Debug.Log("####BROADCAST####");
        //Debug.Log("Broadcast() message:<" + message.Transcript + "> with intensity:" + message.Intensity);
        
        foreach (Listener listener in Listeners)
		{
			float attenuationFactor = 1;
			if(WithinHearingDistance(message, GetListenerPosition(listener), out attenuationFactor)) {
                //Debug.Log("Broadcast() heard by:" + listener.ToString() + " attenuated to: " + attenuationFactor);

                listener.Deliver(DuplicateMessageWithAttenuation(message, attenuationFactor), message.IsNoise);
			}
		}
        //Debug.Log("####BROADCAST_END####");
       // Debug.Log("\n");

    }

    public void Register(Listener listener, Transform listenerTransform)
	{
		listenersTransforms.Add(listener, listenerTransform);
		Register(listener);
	}

	private bool WithinHearingDistance(Message message, Vector3 listenerPosition, out float attenuatedIntensity)
	{
		if (message.EmmiterPosition == Vector3.zero || listenerPosition == Vector3.zero) {
            attenuatedIntensity = 0.0f;
            return false;
        }

        /**
         * Until a distance of 1 unit, there is no attenuation.
         * Beyond 1, the attenuation progresses through a Inverse Square Law.
         * 
         * Lp(R2) = Lp(R1) - 20·Log10(R2/R1)
         *
         * Where: 
         * Lp(R1) = Sound Pressure Level at Initial Location
         * Lp(R2) = Sound Pressure Level at the new Location
         * R1 = Distance from the noise source to initial location
         * R2 = Distance from noise source to the new location
        */

        float distance = Vector3.Distance(message.EmmiterPosition, listenerPosition);

        if (distance <= 1)
        {
            attenuatedIntensity = message.Intensity;
        }
        else
        {
            attenuatedIntensity = message.Intensity - (float)(20 * Math.Log10(distance / 1));
        }

        return attenuatedIntensity > MINIMUM_PERCEPTIBLE_INTENSITY;
	}

	private Vector3 GetListenerPosition(Listener listener)
	{
		Transform transform;
		if (listenersTransforms.TryGetValue (listener, out transform)) {
			return transform.position;
		}
		return Vector3.zero;
	}

	private Message DuplicateMessageWithAttenuation(Message message, float attenuatedIntensity)
	{
        Message messageCopy = new Message();

        messageCopy.EmmiterId = message.EmmiterId;
        messageCopy.EmmiterPosition = message.EmmiterPosition;
        messageCopy.Intensity = attenuatedIntensity;
        messageCopy.Transcript = message.Transcript;
        messageCopy.frequency = message.frequency;
        messageCopy.IsNoise = message.IsNoise;

        return messageCopy;
    }
}
