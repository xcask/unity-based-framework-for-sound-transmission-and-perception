﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MessagingLibrary;
using System;
using Random = UnityEngine.Random;

[RequireComponent(typeof (AudioSource))]
public class PlayerEmitterBehavior : MonoBehaviour {
	[SerializeField] private AudioClip[] m_FootstepSounds;    // an array of footstep sounds that will be randomly selected from.
	[SerializeField] private AudioClip m_JumpSound;           // the sound played when character leaves the ground.
	[SerializeField] private AudioClip m_LandSound;           // the sound played when character touches back on ground.

	private AudioSource m_AudioSource;
	private Emitter mEmitter;

	// Use this for initialization
	void Start () {
		mEmitter = new Emitter(FortressBroadcaster.GetInstance());
		m_AudioSource = GetComponent<AudioSource>();
	}

	// Update is called once per frame
	void Update () {

	}

	public void EmitSound(SOUNDTYPE type) {
		EmitSound(type, null);
	}

	public void EmitSound(SOUNDTYPE type, string transcript) {
		switch (type) {
		case SOUNDTYPE.JUMP_LANDING:
			{
				m_AudioSource.clip = m_LandSound;
				m_AudioSource.Play();
				//EmitSoundMessage(SOUNDTYPE.JUMP, FortressSoundFactory.TRANSCRIPT_JUMP);
				break;
			}
		case SOUNDTYPE.JUMP:
			{
				m_AudioSource.clip = m_JumpSound;
				m_AudioSource.Play();
				//EmitSoundMessage(SOUNDTYPE.JUMP_LANDING, FortressSoundFactory.TRANSCRIPT_JUMP_LANDING);
				break;
			}

		case SOUNDTYPE.FOOTSTEP:
			{
				// pick & play a random footstep sound from the array,
				// excluding sound at index 0
				int n = Random.Range(1, m_FootstepSounds.Length);
				m_AudioSource.clip = m_FootstepSounds[n];
				m_AudioSource.PlayOneShot(m_AudioSource.clip);
				// move picked sound to index 0 so it's not picked next time
				m_FootstepSounds[n] = m_FootstepSounds[0];
				m_FootstepSounds[0] = m_AudioSource.clip;
				//EmitSoundMessage(SOUNDTYPE.FOOTSTEP, FortressSoundFactory.TRANSCRIPT_FOOTSTEP);
				break;
			}
		}

        //TODO remove
        Vector3 pos = transform.position;
        pos.y = 0;
       
		Message messageToEmit = FortressSoundFactory.GetSoundMessage(type, GetEmitterId(), transform.position, false, transcript);

		Emit(messageToEmit);

	}

	private void Emit(Message messageToEmit)
	{
		mEmitter.Emit(messageToEmit);
	}

	private int GetEmitterId() {
		return FortressGlobals.ENTITY_ID_PLAYER;
	}
}