﻿using System.Collections.Generic;

namespace MessagingLibrary
{
    public class Broadcaster
    {
        //FIELDS
        private static Broadcaster singleton;
        
        protected List<Listener> Listeners { get; set; }

        //CONSTRUCTORS AND INSTANTIATION
		protected Broadcaster ()
        {
            Listeners = new List<Listener>();
        }

        public static Broadcaster GetInstance()
        {
            if(singleton == null)
            {
                singleton = new Broadcaster();
            }

            return singleton;
        }

        //INSTANCE METHODS
        /**
         * Registers the listener, which will now receive broadcasts from 
         * this broadcaster.
         * 
         */ 
        public void Register(Listener listener)
        {
            Listeners.Add(listener);
        }

        /**
         * Removes listener from the collection. The listener won't receive
         * any more broadcasts.
         */
        public bool Unregister(Listener listener)
        {
            return Listeners.Remove(listener);
        }

        /**
         * Deliver the message to all the registered listeners.
         */
        public virtual void Broadcast(Message message)
        {
            foreach(Listener listener in Listeners)
            {
                listener.Deliver(message);
            }
        }

        /**
         * Deliver the message to all the registered listeners.
         * The message will be delivered as noise.
         */
        public virtual void BroadcastNoise(Message message)
        {
            message.IsNoise = true;
            foreach (Listener listener in Listeners)
            {
                listener.Deliver(message, true);
            }
        }
    }
}
