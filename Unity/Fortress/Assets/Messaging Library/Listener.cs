﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace MessagingLibrary
{
    public class Listener : Filter
    {
        private Queue<MessageSaliencePair> ReceivedMessages { get; set; }
        private MessageSaliencePair MostSalientPerceivedMessage { get; set; }
        private LinkedList<Message> NoiseMessages { get; set; }
        private LinkedList<Filter> Filters { get; set; }
        private float MinimumPerceptibleIntensity { get; set; }

        private float maxIntensity = 0;
        private float previousGreatest;
        private float greatestRegisteredSalience = 0;
        private bool noisePending = false;

        public Listener (float minimumPerceptibleIntensity = 42)
        {
            ReceivedMessages = new Queue<MessageSaliencePair>();
            NoiseMessages = new LinkedList<Message>();
            Filters = new LinkedList<Filter>();
            MinimumPerceptibleIntensity = minimumPerceptibleIntensity;
            Init();

            greatestRegisteredSalience = 0;
        }

        public virtual void Init()
        {
            Broadcaster.GetInstance().Register(this);
        }

        /**
         * Enqueue a regular, non-continuous message, determining it's intensity, using the 
         * Weighters.
         */
        public virtual void Deliver(Message message)
        {
            Deliver(message, false);
        }

        /**
         * Enqueue a message. If isNoise is true, it is considerer
         * a continuous source of sound.
         */ 
        public void Deliver(Message message, bool isNoise)
        {
            if(isNoise)
            {
                AddNoiseMessage(message);
            }
            else
            {
                ReceivedMessages.Enqueue(new MessageSaliencePair(message, message.Intensity));
            }
        }

        public void ComputeSalience()
        {
            float greatestSalience = MostSalientPerceivedMessage != null ? MostSalientPerceivedMessage.GetSalience() : 0;
            float currentSalience = 0;
            bool greatestRegisteredWasHere = false;
            foreach (MessageSaliencePair pair in ReceivedMessages)
            {
                ComputeSalience(pair);
                currentSalience = pair.GetSalience();
                if (currentSalience >= MinimumPerceptibleIntensity && currentSalience > greatestSalience)
                {
                    MostSalientPerceivedMessage = pair;
                    noisePending = true;
                }

                if(greatestSalience > maxIntensity)
                {
                    maxIntensity = greatestSalience;
                }

                if(currentSalience > greatestRegisteredSalience)
                {
                    previousGreatest = greatestRegisteredSalience;
                    greatestRegisteredSalience = currentSalience;
                    greatestRegisteredWasHere = true;
                }
            }

            if(MostSalientPerceivedMessage != null && noisePending)
            {
                float afterNoiseSalience = ComputeSalience(MostSalientPerceivedMessage.GetMessage(), MostSalientPerceivedMessage.GetSalience());

                if (afterNoiseSalience < MinimumPerceptibleIntensity)
                {
                    if (greatestRegisteredWasHere)
                    {
                        greatestRegisteredSalience = previousGreatest;
                    } else
                    {
                        if (greatestRegisteredSalience == 0)
                            greatestRegisteredSalience = afterNoiseSalience;
                    }
                    MostSalientPerceivedMessage = null;
                } else
                {
                    MostSalientPerceivedMessage.SetSalience(afterNoiseSalience);
                    if(greatestRegisteredWasHere)
                    {
                        greatestRegisteredSalience = afterNoiseSalience;
                    }
                }
                noisePending = false;
            }

            ReceivedMessages.Clear();
        }

        public float GetMax()
        {
            return greatestRegisteredSalience;
        }

        /**
         * Add Noise Message
         */
        protected void AddNoiseMessage(Message noiseMessage)
        {
            //if there is an old version of this noise message, remove it
			StopNoise(noiseMessage);
            //add the noise message pair
            if(noiseMessage.Intensity >= MinimumPerceptibleIntensity)
            {
                NoiseMessages.AddFirst(noiseMessage);
            }
        }

        /**
         * Remove the message representing the noise.
         */ 
        public void StopNoise(Message message)
        {
			foreach(Message currentMessage in NoiseMessages)
            {
				if(currentMessage.EmmiterId == message.EmmiterId)
                {
					NoiseMessages.Remove(currentMessage);
                    break;
                }
            }
        }

        /**
         * Returns the Message Weight pair representing
         * the message with it's corresponding saliency.
         */ 
        private void ComputeSalience(MessageSaliencePair pair)
        {
            float salience = pair.GetSalience();
            Message message = pair.GetMessage();
            foreach (Filter weighter in Filters)
            {
                salience = weighter.ComputeSalience(message, salience);
            }

            pair.SetSalience(salience);
        }

        private void SetFilters(LinkedList<Filter> filters)
        {
            Filters = filters;
        }

        public void AddFilter(Filter filter)
        {
            Filters.AddLast(filter);
        }

        public bool HasMessages()
        {
            return MostSalientPerceivedMessage != null;
        }

        /**
         * Get the perceived message
         */ 
        public Message PopMessage()
        {
            Message message = MostSalientPerceivedMessage.GetMessage();
            MostSalientPerceivedMessage = null;

            return message;
        }

        public float ComputeSalience(Message message, float currentSalience)
        {
            float highestIntensityNoise = 0;
            foreach (Message noise in NoiseMessages)
            {
                if(noise.Intensity > highestIntensityNoise)
                {
                    highestIntensityNoise = noise.Intensity;
                }
            }

            if (highestIntensityNoise > currentSalience)
            {
                return currentSalience - (highestIntensityNoise - currentSalience);
            }

            return currentSalience - ((highestIntensityNoise - currentSalience) * 0.3f);
        }
    }
}
