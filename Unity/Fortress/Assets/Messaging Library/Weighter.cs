﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MessagingLibrary
{
    public interface Filter
    {
        float ComputeSalience(Message message, float currentSalience);
    }
}
