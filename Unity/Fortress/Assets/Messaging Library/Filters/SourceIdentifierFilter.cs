﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MessagingLibrary.Filters
{
    public class SourceIdentifierFilter : Filter
    {
        private int age;
        public Dictionary<int, float> prioritiesMap;

        public float ComputeSalience(Message message, float currentSalience)
        {
            if (message.EmmiterId != Emitter.INVALID_ID || message.EmmiterId != Emitter.DEFAULT_ID || !prioritiesMap.ContainsKey(message.EmmiterId))
                return currentSalience;
            int i = message.EmmiterId;
            float priorityValue;

            prioritiesMap.TryGetValue(i, out priorityValue);

            return currentSalience * priorityValue;

        }

        public void SetVoicesPrioritiesMap(Dictionary<int, float> prioritiesMap)
        {
            this.prioritiesMap = prioritiesMap;
        }
    }
}
