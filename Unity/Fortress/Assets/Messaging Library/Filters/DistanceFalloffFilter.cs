﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MessagingLibrary.Filters
{
    public class DistanceFalloffFilter : Filter
    {
		private PositionProvider mProvider;

        public float ComputeSalience(Message message, float currentWeight)
        {
			Vector3 listenerPosition = mProvider.GetPosition();
            double distance = Math.Sqrt(Math.Pow((message.EmmiterPosition.x - listenerPosition.x), 2.0) + Math.Pow((message.EmmiterPosition.z - listenerPosition.z), 2.0));

            if(distance < 40)
            {
                return currentWeight * 1.0f;
            }
            else if(distance < 70)
            {
                return currentWeight * 0.5f;
            }
            else if(distance < 100)
            {
                return currentWeight * 0.2f;
            }
            else
            {
                return currentWeight * 0.0f;
            }

        }

        public void SetPositionProvider(PositionProvider provider)
        {
			this.mProvider = provider;
        }

		public interface PositionProvider {
			Vector3 GetPosition();
		}
    }
}
