﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MessagingLibrary.Filters
{
    public class FocusFilter : Filter
    {
        private int focusLevel;
        private float contribution = 0.1f;

        public float ComputeSalience(Message message, float currentSalience)
        {
            float attenuation = currentSalience * (contribution * GetAttenuation(focusLevel));

            return currentSalience - attenuation;
        }

        public void ChangeFocusLevel(int focusLevel)
        {
            this.focusLevel = focusLevel;
        }

        private float GetAttenuation(int focusLevel)
        {
            switch(focusLevel)
            {
                case 1:
                    return 0.0f;
                case 2:
                    return 0.25f;
                case 3:
                    return 0.5f;
                case 4:
                    return 0.75f;
                case 5:
                    return 1.0f;
                default:
                    return 1.0f;

            }
        }
    }
}
