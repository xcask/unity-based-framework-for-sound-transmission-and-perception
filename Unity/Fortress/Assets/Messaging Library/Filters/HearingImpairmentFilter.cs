﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MessagingLibrary.Filters
{
    public class HearingImpairmentFilter : Filter
    {
        private bool isDeafLeftEar = false;
        private bool isDeafRightEar = false;

        public float ComputeSalience(Message message, float currentSalience)
        {
            if (isDeafLeftEar && isDeafRightEar)
            {
                return 0.0f;
            }
            else
            {
                return currentSalience;
            }
        }

        public void SetDeaf(bool leftEar, bool rightEar)
        {
            this.isDeafLeftEar = leftEar;
            this.isDeafRightEar = rightEar;
        }
    }
}
