﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MessagingLibrary.Filters
{
    public class AgeFilter : Filter
    {
        private int age;

        public float ComputeSalience(Message message, float currentSalience)
        {
            float attenuationFactor = CalculateAttenuation(currentSalience, age, message.frequency);

            return currentSalience * attenuationFactor;
        }

        public void SetAge(int age)
        {
            this.age = age;
        }

		public float CalculateAttenuation(float intensity, int age, FrequencyRange frequency) {
            float attenuation = (float)(FrequencyCoefficient(frequency) * Math.Pow((age - 18), 2));

            return 1f - (0.01f * attenuation); 
		}

		public double FrequencyCoefficient(FrequencyRange frequency) {
            switch(frequency)
            {
                case FrequencyRange.FemaleVoice:
                    return 0.00120f;
                case FrequencyRange.MaleVoice:
                    return 0.00124f;
                case FrequencyRange.ShoesOnConcrete:
                    return 0.00120f;
                case FrequencyRange.Music:
                    return 0.00145f;
                case FrequencyRange.FourK:
                    return 0.00220f;
                default:
                    return 0.00128f;
            }
        }

    }
}
