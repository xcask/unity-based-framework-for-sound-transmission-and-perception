﻿using System;
using System.Collections.Generic;

namespace MessagingLibrary
{
    public class Emitter
    {
        public const int DEFAULT_ID = 0;
        public const int INVALID_ID = -1;
        private int emmiterId = DEFAULT_ID;
        public Broadcaster LinkedBroadcaster { get; set; }

        /*
         * This emitter emits to the singleton broadcaster.
         */
        public Emitter() {
			LinkedBroadcaster = Broadcaster.GetInstance();
        }

        /*
         * This emitter emits to the specified broadcaster.
         */ 
        public Emitter(Broadcaster broadcaster)
        {
			LinkedBroadcaster = broadcaster;
        }

        /**
         * Send message to the associated broadcaster.
         */ 
        public void Emit(Message message)
        {
			LinkedBroadcaster.Broadcast(message);
        }

        /*
         * Send message to a different broadcaster
         */ 
        public void Emit(Message message, Broadcaster broadcaster)
        {
            message.EmmiterId = this.emmiterId;
			broadcaster.Broadcast(message);
        }

        /*
         * Set the emitter's identifier
         */ 
        public void SetEmmiterId(int emmiterId)
        {
            this.emmiterId = emmiterId;
        }
    }
}
