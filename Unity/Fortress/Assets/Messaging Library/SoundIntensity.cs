﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MessagingLibrary
{
    public class SoundIntensity
    {
        /**
         * Constants that represent the mean intensity of 
         * the corresponding sounds, measured in dB
         */ 
        public const float NATURAL_CONVERSATION = 60.0f;
        public const float SHOUT = 88.0f;
        public const float LIGHT_MACHINERY = 90.0f;
        public const float CONCERT = 120.0f;
        public const float HARMFUL_SOUND = 150.0f;
    }
}
