﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MessagingLibrary
{
    public class MessageSaliencePair
    {
        //A message's salience value interval is ]0, oo[
        //The message's intensity is the default salience of a message.
        //The closer to 0, the less audible is the message
        float salience = 1;
        Message message;

        public MessageSaliencePair(Message message)
        {
            this.message = message;
            this.salience = message.Intensity;
        }

        public MessageSaliencePair(Message message, float salience)
        {
            this.message = message;
            this.salience = salience;
        }

        public void SetSalience(float salience)
        {
            this.salience = salience;
        }

        public float GetSalience()
        {
            return this.salience;
        }

        public Message GetMessage()
        {
            return this.message;
        }
    }
}
