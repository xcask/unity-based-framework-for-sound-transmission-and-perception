﻿using System;
using UnityEngine;

namespace MessagingLibrary
{
    public class Message
    {
        public string Transcript { get; set; }
        public int EmmiterId { get; set; }
        public Vector3 EmmiterPosition { get; set; }
        public float Intensity { get; set; }
        public bool IsNoise { get; set; }
		public FrequencyRange frequency { get; set; }

        public int FrequencyValue(FrequencyRange frequencyRange)
        {
            switch(frequencyRange)
            {
                case FrequencyRange.MaleVoice:
                    {
                        return 6000;
                    }
                case FrequencyRange.FemaleVoice:
                    {
                        return 7000;
                    }
                case FrequencyRange.Siren:
                    {
                        return 18000;
                    }
                case FrequencyRange.Music:
                    {
                        return 10000;
                    }
                case FrequencyRange.ShoesOnConcrete:
                    {
                        return 250;
                    }
                default:
                    return 0;
            }
        }
    }

	public enum FrequencyRange {MaleVoice, FemaleVoice, Siren, Music, ShoesOnConcrete, FourK};
}
