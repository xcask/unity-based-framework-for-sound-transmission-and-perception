﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MessagingLibrary;
using System;



public class EmitterBehavior : MonoBehaviour {
	Emitter mEmitter;
    LinkedList<Message> messagesToEmit;

	[SerializeField] private AudioClip[] m_FootstepSounds;    // an array of footstep sounds that will be randomly selected from.
	[SerializeField] private AudioClip m_JumpSound;           // the sound played when character leaves the ground.
	[SerializeField] private AudioClip m_LandSound;           // the sound played when character touches back on ground.

	// Use this for initialization
	void Start () {
		mEmitter = new Emitter();
    }

	// Update is called once per frame
	void Update () {
        
    }

    public void Emit(Message messageToEmit)
    {

        mEmitter.Emit(messageToEmit);
    }
}
