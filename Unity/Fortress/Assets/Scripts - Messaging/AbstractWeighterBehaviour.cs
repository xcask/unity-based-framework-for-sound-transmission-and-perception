﻿using UnityEngine;
using System.Collections;
using MessagingLibrary;

public abstract class AbstractFilterBehaviour : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public abstract Filter GetFilter();
}