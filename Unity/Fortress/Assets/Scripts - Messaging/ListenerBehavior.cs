﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MessagingLibrary;

public class ListenerBehavior : MonoBehaviour {
    private const int INTERVAL_BETWEEN_SOUND_PERCEPTION = 200; //milliseconds
    private const int FINAL_PRINT = 8; //seconds
    Callback mCallback;
	Listener mListener;
    public float minimumPerceptibleSoundIntensity = 0;
    private float milliseconds;
    private float secondsForPrint;
    public string name;
    private bool printed = false;

    // Use this for initialization
    void Start () {
		mListener = new LoggingListener(transform, minimumPerceptibleSoundIntensity);
        AbstractFilterBehaviour[] weightersComponents = gameObject.GetComponents<AbstractFilterBehaviour>();

        foreach (AbstractFilterBehaviour weighterBehaviour in weightersComponents)
        {
            if(weighterBehaviour.isActiveAndEnabled)
                mListener.AddFilter(weighterBehaviour.GetFilter());
        }

        milliseconds = INTERVAL_BETWEEN_SOUND_PERCEPTION;
        secondsForPrint = FINAL_PRINT;
    }
	
	// Update is called once per frame
	void Update () {
        mListener.ComputeSalience();
        milliseconds -= Time.deltaTime * 100;
        secondsForPrint -= Time.deltaTime;
        if (milliseconds <= 0)
        {
            milliseconds = INTERVAL_BETWEEN_SOUND_PERCEPTION;
            if (mListener.HasMessages())
            {
                if (mCallback != null)
                {
                    mCallback.MessagePerceived(mListener.PopMessage());
                }
            }
        }

        if(secondsForPrint <= 0 && !printed)
        {
            Debug.Log("LISTENER " + name + " max:" + mListener.GetMax());
            printed = true;
        }
	}

	public void RegisterCallback(Callback callback) {
		mCallback = callback;
	}

    private Transform GetTransform()
    {
        return transform;
    }
	public interface Callback
	{
		void MessagePerceived(Message message);
	}

    public class LoggingListener : Listener
    {
        public LoggingListener(Transform transform, float minimumPerceptibleIntensity) : base(minimumPerceptibleIntensity)
        {
            FortressBroadcaster.GetInstance().Register(this, transform);
        }

        public override void Deliver(Message message)
        {
            base.Deliver(message);
        }
    }
}
