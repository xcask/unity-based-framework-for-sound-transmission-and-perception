﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
using MessagingLibrary;

[RequireComponent(typeof(ListenerBehavior))]
[RequireComponent(typeof(ControllerBehavior))]
public class CognitionBehavior : MonoBehaviour, ListenerBehavior.Callback {
    //private int newState = STATE_INVALID;
    private const int STATE_INVALID = -1;
    private const int STATE_IDLE = 0;
    private const int STATE_PATROL = 1;
    private const int STATE_CHECK = 2;
    private const int STATE_CHASE = 3;
    private const int STATE_DETAIN = 4;

    private ListenerBehavior listenerBehavior;
    private ControllerBehavior characterControl;

    private int state = STATE_IDLE;
    private Vector3 soundHeardPosition;
    private bool newSoundHeard = false;
    private GameObject player;
    private CustomFirstPersonController playerController;
    private FocusFilterBehaviour focusFilterBehaviour;
    private GameObject mGameoverPanel;
    private TextMesh textMesh;


    public Transform[] points;
    private int destPoint = 0;

    // Use this for initialization
    void Start() {
        listenerBehavior = GetComponent<ListenerBehavior>();
        characterControl = GetComponent<ControllerBehavior>();
        focusFilterBehaviour = GetComponent<FocusFilterBehaviour>();
        player = GameObject.Find("Player");
        textMesh = GetComponentInChildren<TextMesh>();

        playerController = player.GetComponent<CustomFirstPersonController>();
        mGameoverPanel = GameObject.Find("GameOverPanel");
        mGameoverPanel.SetActive(false);

        listenerBehavior.RegisterCallback(this);
        state = STATE_PATROL;
        focusFilterBehaviour.ChangeFocusLevel(2);
        NextPatrolPoint();

    }

    // Update is called once per frame
    void Update() {
        switch (state) {
            case STATE_IDLE:
                {
                    //Does nothing
                    break;
                }
            case STATE_PATROL:
                {
                    if (!characterControl.agent.pathPending && characterControl.agent.remainingDistance < 0.5f) {
                        NextPatrolPoint();
                    }

                    break;
                }
            case STATE_CHECK:
                {
                    if (newSoundHeard) {
                        characterControl.agent.destination = soundHeardPosition;
                        newSoundHeard = false;
                        break;
                    }
                    if (PlayerIsInSight()) {
                        state = STATE_CHASE;
                        textMesh.text = "!";

                        focusFilterBehaviour.ChangeFocusLevel(3);
                    }
                    break;
                }
            case STATE_CHASE:
                {
                    textMesh.transform.LookAt(Camera.main.transform);
                    if (characterControl.agent.remainingDistance < 1.0f) {
                        state = STATE_DETAIN;
                    }

                    characterControl.target = player.transform;
                    break;
                }
            case STATE_DETAIN:
                {
                    focusFilterBehaviour.ChangeFocusLevel(1);
                    state = STATE_PATROL;
                    break;
                }
            default:
                {
                    state = STATE_IDLE;
                    break;
                }
        }
    }

    void NextPatrolPoint() {
        if (points == null || points.Length == 0) {
            state = STATE_IDLE;
            return;
        }

        if(points[destPoint] == null)
        {
            state = STATE_IDLE;
            return;
        }

		characterControl.agent.destination = points[destPoint].position;
		destPoint = (destPoint + 1) % points.Length;
	}

	private bool PlayerIsInSight() {
		RaycastHit hit;
		Vector3 rayDirection = player.transform.position - transform.position;
		if (Physics.Raycast (transform.position, rayDirection, out hit)) {
			return hit.transform == player.transform;
		}

		return false;
	}

    void ListenerBehavior.Callback.MessagePerceived(Message message)
    {
        if (message.EmmiterId == FortressGlobals.ENTITY_ID_PLAYER)
        {
            soundHeardPosition = new Vector3(message.EmmiterPosition.x, message.EmmiterPosition.y, message.EmmiterPosition.z);
            newSoundHeard = true;
            state = STATE_CHECK;
            focusFilterBehaviour.ChangeFocusLevel(1);
        }
    }
}
